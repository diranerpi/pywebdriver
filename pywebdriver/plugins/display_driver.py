# -*- coding: utf-8 -*-
###############################################################################
#
#   Copyright (C) 2014-2016 Akretion (http://www.akretion.com).
#   @author Sébastien BEAU <sebastien.beau@akretion.com>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from pywebdriver import app, config, drivers
from flask_cors import cross_origin
import subprocess
from flask import request, jsonify, render_template
import ast
from base_driver import ThreadDriver, check
import simplejson
from lcdmtrx import LcdMatrix
import sys
import time

meta = {
    'name': "POS Display",
    'description': """This plugin add the support of customer display for your
        pywebdriver""",
    'require_pip': ['pyposdisplay'],
    'require_debian': ['python-pyposdisplay'],
}

try:
    import pyposdisplay
except:
    installed = False
else:
    AUTHOR = [
        ([u'PyWebDriver', u'By'], 2),
        ([u'Sylvain CALADOR', u'@ Akretion'], 1.5),
        ([u'Sébastien BEAU', u'@ Akretion'], 1.5),
        ([u'Sylvain LE GAL', u'@ GRAP'], 1.5),
        ([u'dirane', u'@ MobileIT'], 1.5),
        ([u'Status:', u'OK'], 5),
    ]
    installed = True

    class DisplayDriver(ThreadDriver, pyposdisplay.Driver):
        """ Display Driver class for pywebdriver """

        def __init__(self, *args, **kwargs):
            ThreadDriver.__init__(self)
            pyposdisplay.Driver.__init__(self, *args, **kwargs)
            # TODO FIXME (Actually hardcoded, but no possibility to know
            # the model easily
            self.vendor_product = '1504_11'

        @app.route('/display_status.html', methods=['GET'])
        @cross_origin()
        def display_status_http():
            for line, duration in AUTHOR:
                display_driver.push_task('send_text', line)
                time.sleep(duration)
            return render_template('display_status.html')

        def get_status(self):
            try:
                self.set_status('connected')
                # When I use Odoo POS v8, it regularly displays
                # "PyWebDriver / PosBox Status" on the LCD !!!
                # So I comment the line below -- Alexis de Lattre
                # display_driver.push_task(
                #    'send_text', [_(u'PyWebDriver'), _(u'PosBox Status')])
                # TODO Improve Me
                # For the time being, it's not possible to know if the display
                # is 'disconnected' in 'error' state
                # Maybe could be possible, improving pyposdisplay library.
            except Exception:
                pass
            return self.status

    driver_config = {}
    if config.get('display_driver', 'device_name'):
        driver_config['customer_display_device_name'] =\
            config.get('display_driver', 'device_name')
    if config.getint('display_driver', 'device_rate'):
        driver_config['customer_display_device_rate'] =\
            config.getint('display_driver', 'device_rate')
    if config.getfloat('display_driver', 'device_timeout'):
        driver_config['customer_display_device_timeout'] =\
            config.getfloat('display_driver', 'device_timeout')
    driver_name = 'bixolon'
    if config.has_option('display_driver', 'driver_name'):
        driver_name = config.get('display_driver', 'driver_name')

    display_driver = DisplayDriver(driver_config, use_driver_name=driver_name)
    drivers['display_driver'] = display_driver


@app.route(
    '/hw_proxy/send_text_customer_display',
    methods=['POST', 'GET', 'PUT', 'OPTIONS'])
@cross_origin(headers=['Content-Type'])
@check(installed, meta)
def send_text_customer_display():
    app.debug = True
    app.logger.debug('LCD: Call send_text')
    text_to_display = request.json
    lines = text_to_display.get("text_to_display")
    app.logger.debug('LCD: lines=%s', lines)
    line1 =  ast.literal_eval(str(tuple(map(str, lines))))[0]
    line2 =  ast.literal_eval(str(tuple(map(str, lines))))[1] 
#    newline = str(tuple(map(str, lines))) 

    app.logger.debug("%s and  %s"  ,line1, line2)  
    do_lcd_matrix_test(line1, line2) 
#    subprocess.Popen(['python /home/pi/odoo/pywebdriver/pywebdriver/plugins/display.sh %s' % line1], shell = True)
#    display_driver.push_task('send_text', lines)
    return jsonify(jsonrpc='2.0', result=True)



@app.route("/json", methods=['GET','POST'])
def json():
    
    app.logger.debug("JSON received...")
    app.logger.debug(request.json)
    
    if request.json:
        mydata = request.json # will be 
        
        return "Thanks. Your age is %s" % mydata.get("age")

    else:
        return "no json received"


PORT_SERIE = '/dev/ttyACM0' #identification du port série sur lequel le LCD USB est connecté

LCD_COLS = 16 # Taille du LCD 16 caractères x 2 lignes
LCD_ROWS = 2

def do_lcd_matrix_test(line1 , line2):
	lcd = LcdMatrix( PORT_SERIE )
	
	# Initialiser la taille du LCD (et sauver dans l'EEPROM)
	lcd.set_lcd_size( LCD_COLS, LCD_ROWS )
	lcd.clear_screen()
	
	# Activer/désactiver le rétro-éclairage
	lcd.activate_lcd( True );
	
	# Constrat par défaut
	lcd.contrast()

	# Luminosité max + couleur RGB
	lcd.brightness( 255 )
	
	# Couleur RBG 
	lcd.color( 255, 17, 30 )
    	
	# Position d'origine
	lcd.clear_screen()
	
	# Tester avec le retour à la ligne
	# \r fait un retour à ligne et est insensible à la valeur de autoscroll.
	lcd.autoscroll( False )
	lcd.clear_screen()
        line1 = line1[:-1] 
#        line1 = "toto1          "
#        line2 = "toto2           "
#        lcd.write( line1+'\rtoto' ) 
        lcd.write( line1+'\r'+line2 )
#	lcd.write( "Ligne 1\rLigne 2" )

	

